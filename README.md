

## Installation

- clone the repo: `git clone https://gitlab.com/gtemel/holiday-calendar.git`
- go to project folder: `cd holiday-calendar`
- create .env file from example: `cp .env.example .env`
- install composer dependencies: `composer install`
- create app key: `php artisan key:generate`
- edit your database credentials in .env file, create database `holiday_app` and migrate: `php artisan migrate`
- install frontend dependencies: `yarn`
- run the application and visit the site: `vite`

## Tests

`* php artisan test`


### List holidays
![1.png](1.png)

### Import holidays
![2.png](2.png)

### Edit holidays
![3.png](3.png)