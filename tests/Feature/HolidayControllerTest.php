<?php

namespace Tests\Feature;

use App\Http\Controllers\HolidayController;
use App\Models\Holiday;
use App\Services\HolidayApiService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tests\TestCase;

class HolidayControllerTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    public function testGetCountriesSuccess()
    {
        $mockService = $this->createMock(HolidayApiService::class);
        $mockService->method('getCountries')->willReturn(['US', 'UK']);

        $controller = new HolidayController();
        $response = $controller->getCountries($mockService);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->status());
        $this->assertEquals(['US', 'UK'], $response->getData(true));
    }

    public function testGetApiHolidaysSuccess()
    {
        $mockService = $this->createMock(HolidayApiService::class);
        $mockService->method('getHolidays')->willReturn([
            ['date' => '2024-01-01', 'name' => 'New Year'],
        ]);

        $controller = new HolidayController();
        $response = $controller->getApiHolidays($mockService, 'US', 2024);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->status());
        $this->assertEquals([['date' => '2024-01-01', 'name' => 'New Year']], $response->getData(true));
    }

    public function testStoreSuccess()
    {
        $holidayData = [
            'holidays' => [
                ['date' => '2024-01-01', 'localName' => 'New Year', 'countryCode' => 'US'],
            ],
        ];

        $controller = new HolidayController();
        $request = new Request($holidayData);
        $response = $controller->store($request);

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->status());
        $this->assertEquals('Holidays imported successfully', $response->getData(true)['message']);
        $this->assertDatabaseHas('holidays', ['date' => '2024-01-01', 'name' => 'New Year', 'country_code' => 'US']);
    }

    public function testIndexSuccess()
    {
        // Create a sample holiday
        Holiday::create([
            'date' => '2024-01-01',
            'name' => 'New Year',
            'country_code' => 'US',
            'year' => 2024,
        ]);

        $controller = new HolidayController();
        $response = $controller->index();

        $this->assertInstanceOf(JsonResponse::class, $response);
        $this->assertEquals(200, $response->status());
        $data = $response->getData(true);
        $this->assertCount(1, $data);
        $this->assertEquals('New Year', $data[0]['name']);
    }

    public function testUpdateSuccess()
    {
        $holiday = Holiday::create([
            'date' => '2024-01-01',
            'name' => 'New Year',
            'country_code' => 'US',
            'year' => 2024,
        ]);

        $updateData = [
            'date' => '2024-01-01',
            'name' => 'Updated New Year',
            'notes' => 'This is a test note.',
        ];

        $response = $this->json('PUT', "/api/holidays/update/{$holiday->id}", $updateData);

        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Holiday updated successfully',
            ]);

        $this->assertDatabaseHas('holidays', [
            'id' => $holiday->id,
            'name' => 'Updated New Year',
            'notes' => 'This is a test note.',
        ]);
    }
}
