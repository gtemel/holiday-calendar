import './bootstrap'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-sugar.css'
import VueDatePicker from '@vuepic/vue-datepicker'
import '@vuepic/vue-datepicker/dist/main.css'
import App from './components/App.vue'

const app = createApp(App)
app.use(createPinia())

app.use(VueToast, {
  position: 'top-right',
  duration: 5000
})
app.component('VueDatePicker', VueDatePicker)

app.mount('#app')
