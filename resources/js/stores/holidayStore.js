import { ref } from 'vue'
import { defineStore } from 'pinia'
import axios from 'axios'
import customParseFormat from 'dayjs/plugin/customParseFormat'
import dayjs from 'dayjs'
import 'dayjs/locale/tr'

dayjs.extend(customParseFormat)
dayjs.locale('tr')

export const useHolidayStore = defineStore('holiday', () => {
  const countries = ref([])
  const apiHolidays = ref([])
  const selectedHolidays = ref([])
  const countriesLoading = ref(false)
  const holidaysLoading = ref(false)
  const holidays = ref([])
  const currentView = ref('holidayForm')

  const setCurrentView = (view) => {
    currentView.value = view
  }

  const fetchCountries = async () => {
    countriesLoading.value = true
    try {
      const response = await axios.get('/api/countries')
      countries.value = response.data
    } catch (e) {
      console.error('Error fetching countries:', e)
    } finally {
      countriesLoading.value = false
    }
  }

  const fetchApiHolidays = async (countryCode, year) => {
    holidaysLoading.value = true
    try {
      const response = await axios.get(`/api/holidays/${countryCode}/${year}`)
      apiHolidays.value = response.data
    } catch (e) {
      if (e.response && e.response.status === 422 && e.response.data.errors) {
        const errors = e.response.data.errors
        const errorMessages = Object.values(errors).flat().join(' ')
        throw new Error(errorMessages)
      } else {
        throw new Error('Failed to fetch holidays.')
      }
    } finally {
      holidaysLoading.value = false
    }
  }

  const importHolidays = async () => {
    try {
      await axios.post('/api/holidays/import', { holidays: selectedHolidays.value })
      selectedHolidays.value = []
    } catch (e) {
      console.error('Error importing holidays:', e)
      throw new Error(e.response.data.message || 'Failed to import holidays')
    }
  }

  const fetchHolidays = async () => {
    try {
      const response = await axios.get('/api/holidays')
      holidays.value = response.data
    } catch (e) {
      console.error('Error fetching imported holidays:', e)
    }
  }

  const updateHoliday = async (id, holidayDetails) => {
    try {
      const formattedDate = dayjs(holidayDetails.date, 'DD MMMM YYYY, dddd', 'tr').format('YYYY-MM-DD')

      if (!dayjs(formattedDate).isValid()) {
        throw new Error('Invalid date format.')
      }
      const payload = {
        ...holidayDetails,
        date: formattedDate
      }
      const response = await axios.put(`/api/holidays/update/${id}`, payload)
      await fetchHolidays()
      return response.data
    } catch (error) {
      console.error('Error updating holiday:', error)
      throw error
    }
  }

  const formatListDate = (dateString) => {
    const dateOptions = {
      year: 'numeric',
      month: 'long',
      day: '2-digit'
    }
    const weekdayOptions = {
      weekday: 'long'
    }

    const formattedDate = new Date(dateString).toLocaleDateString('tr-TR', dateOptions)
    const weekday = new Date(dateString).toLocaleDateString('tr-TR', weekdayOptions)

    return `${formattedDate}, ${weekday}`
  }

  return {
    countries,
    apiHolidays,
    selectedHolidays,
    countriesLoading,
    holidaysLoading,
    fetchCountries,
    fetchApiHolidays,
    importHolidays,
    holidays,
    fetchHolidays,
    updateHoliday,
    currentView,
    setCurrentView,
    formatListDate
  }
})
