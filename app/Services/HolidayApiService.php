<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

/**
 * Class HolidayApiService
 *
 * Provides access to holiday data using an external API.
 */
class HolidayApiService
{
    /**
     * Base URL for the external API.
     *
     * @var string
     */
    protected $baseUrl;

    /**
     * HolidayApiService constructor
     *
     * Initializes the class and sets baseUrl from configuration.
     */
    public function __construct()
    {
        $this->baseUrl = config('services.date_nager.base_url');
    }

    /**
     * Retrieve a list of available countries from the external API.
     *
     * @return array
     */
    public function getCountries()
    {
        return Cache::remember('available-countries', 60 * 24, function () {
            return $this->makeRequest('v3/AvailableCountries');
        });
    }

    /**
     * Get public holidays for a specific country and year from the external API.
     *
     * @param  string  $countryCode
     * @param  int  $year
     * @return array
     */
    public function getHolidays($countryCode, $year)
    {
        $cacheKey = "public-holidays-{$countryCode}-{$year}";

        return Cache::remember($cacheKey, 60 * 24, function () use ($countryCode, $year) {
            return $this->makeRequest("v3/PublicHolidays/$year/$countryCode");
        });
    }

    /**
     * Makes a GET request to the specified endpoint and returns the response.
     *
     * @param string $endpoint The API endpoint to make the request to.
     * @return array The response data as an associative array.
     */
    protected function makeRequest($endpoint)
    {
        $response = Http::acceptJson()->get($this->baseUrl.$endpoint);

        if ($response->successful()) {
            return $response->json();
        }

        \Log::error("Error fetching data from Date Nager API: {$response->body()}");

        return [];
    }
}
