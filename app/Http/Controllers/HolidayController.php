<?php

namespace App\Http\Controllers;

use App\Models\Holiday;
use App\Services\HolidayApiService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

/**
 * Class HolidayController
 *
 * @package App\Http\Controllers
 */
class HolidayController extends Controller
{
    /**
     * Retrieves list of countries from HolidayApiService and returns as a JsonResponse.
     *
     * @param  HolidayApiService  $holidayService
     * @return JsonResponse
     */
    public function getCountries(HolidayApiService $HolidayApiService)
    {
        return response()->json($HolidayApiService->getCountries());
    }

    /**
     * Get holidays for a specific country and year from the Holidays API
     *
     * @param  string  $countryCode
     * @param  int  $year
     * @return \Illuminate\Http\JsonResponse
     */
    public function getApiHolidays(HolidayApiService $HolidayApiService, $countryCode, $year)
    {
        $validator = Validator::make(['countryCode' => $countryCode, 'year' => $year], [
            'countryCode' => 'required|size:2',
            'year' => 'required|integer|min:1900|max:'.Carbon::now()->year,
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => false, 'errors' => $validator->messages()], 422);
        }

        return response()->json($HolidayApiService->getHolidays($countryCode, $year));
    }

    /**
     * Import holidays data to database
     *
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'holidays.*.date' => 'required|date',
            'holidays.*.localName' => 'required|string|max:255',
            'holidays.*.countryCode' => 'required|size:2',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        try {
            foreach ($request->input('holidays') as $holidayData) {
                $date = new Carbon($holidayData['date']);

                // Check if the holiday already exists to avoid duplicates
                $existingHoliday = Holiday::where('date', $holidayData['date'])
                    ->where('country_code', $holidayData['countryCode'])
                    ->first();

                if (! $existingHoliday) {
                    Holiday::create([
                        'date' => $holidayData['date'],
                        'name' => $holidayData['localName'],
                        'country_code' => $holidayData['countryCode'],
                        'year' => $date->year,
                    ]);
                }
            }

            return response()->json(['message' => 'Holidays imported successfully']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Error importing holidays: '.$e->getMessage()], 500);
        }
    }

    /**
     * Get all stored holidays
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $holidays = Holiday::all();

        return response()->json($holidays);
    }

    /**
     * Update the specified holiday in database.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|date_format:Y-m-d',
            'name' => 'required|string|max:255',
            'notes' => 'sometimes|string|nullable',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $holiday = Holiday::find($id);

        if (! $holiday) {
            return response()->json(['message' => 'Holiday not found'], 404);
        }

        $holiday->update($validator->validated());

        return response()->json(['message' => 'Holiday updated successfully', 'holiday' => $holiday]);
    }
}
