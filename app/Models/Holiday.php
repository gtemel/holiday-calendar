<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Holiday extends Model {
    use HasFactory;

    protected $fillable = [
        'date',
        'name',
        'notes',
        'country_code',
        'year'
    ];

    protected $casts = [
        'date' => 'date',
        'year' => 'integer',
    ];
}
