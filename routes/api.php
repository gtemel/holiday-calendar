<?php

use App\Http\Controllers\HolidayController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/countries', [HolidayController::class, 'getCountries']);

Route::prefix('holidays')->group(function () {
    Route::get('/{countryCode}/{year}', [HolidayController::class, 'getApiHolidays']);
    Route::post('/import', [HolidayController::class, 'store']);
    Route::put('/update/{id}', [HolidayController::class, 'update']);
    Route::get('/', [HolidayController::class, 'index']);
});
